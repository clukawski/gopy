package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"sync"
)

func ConCopy(file string, dest string, fod bool, wg *sync.WaitGroup) {
    var final string
    switch fod {
    case true:
        final = dest + file
    case false:
        final = dest
    }

    //Open file, check if it exists
	filein, err := os.Open(file)
	if err != nil {
        panic(err)
    }
	defer func() {
        if err := filein.Close(); err != nil {
            panic(err)
        }
    }()

    //Create destination file
    fileout, err := os.Create(final)
    if err != nil {
        panic(err)
    }
    defer func() {
        if err := fileout.Close(); err != nil {
            panic(err)
        }
    }()

    buf := make([]byte, 1024)
    //write chunks
	for {
        n, err := filein.Read(buf)
        if err != nil && err != io.EOF {
            panic(err)
        }
        if n == 0 { break }

        if _, err := fileout.Write(buf[:n]); err != nil {
            panic(err)
        }
    }

    wg.Done()
}

func FileOrDir(dest string) bool {
    var fod bool
    // let's check if the last argument is a directory before we do anything silly
    filein, err := os.Open(dest)
    if err != nil {
        return false
    }

    defer filein.Close()
    fileinf, err := filein.Stat() //gets filein's FileInfo, stores in fileinf
    if err != nil {
        panic(err)
    }

    switch mode := fileinf.Mode(); {
    case mode.IsDir(): //is directory?
        fod = true
    case mode.IsRegular(): //is regular file?
        fod = false
    }

    return fod
}

func main() {
    var fod bool
	flag.Parse()

	var wg sync.WaitGroup
    narg := flag.NArg() //number of arguments
    fod  = FileOrDir(flag.Arg(flag.NArg()-1))

    switch fod {
    case true:
        fmt.Println("Copying...")
    case false:
        if narg > 2 {
            fmt.Println("More than one file and no directory specification")
            os.Exit(1)
        }
        fmt.Println("Copying...")
    }

    for i := 0; i < narg-1; i++ {
		wg.Add(1)
		go conCopy(flag.Arg(i), flag.Arg(flag.NArg()-1), fod, &wg)
	}

	wg.Wait()
	fmt.Println("Done.")
}
