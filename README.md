# gopy

[Documentation online](http://godoc.org/bitbucket.org/lukawskc/gopy)

**gopy** is a command line tool similar to cp, used to copy multiple files concurrently

## Install (with GOPATH set on your machine)
----------

* Step 1: Get the package. Then you will be able to use `gopy` as an executable. 

```
go get bitbucket.org/lukawskc/gopy
```
* Step 2: Use!

##Usage
-------
```
$ gopy [file1] [file2] [filex] [destination]
```

##License
---------
gopy is BSD licensed.